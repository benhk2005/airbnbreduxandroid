package com.benleungcreative.airbnbsignup

import android.graphics.Color
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Patterns
import androidx.annotation.ColorInt
import androidx.core.text.toSpannable
import com.benleungcreative.airbnbsignup.helper.PasswordStrength
import org.rekotlin.Action


fun mainReducer(action: Action, state: AppState?): AppState {
    var state = state ?: AppState()
    when (action) {
        is EmailUpdate -> {
            state = resetEmailState(action.email, state)
            state = processStateForPassword(action.firstName, action.lastName, action.password, state, false, true)
        }
        is LastNameUpdate -> {
            state = resetLastNameState(action.lastName, state)
            state = processStateForPassword(action.firstName, action.lastName, action.password, state, false, true)
        }
        is FirstNameUpdate -> {
            state = resetFirstNameState(action.firstName, state)
            state = processStateForPassword(action.firstName, action.lastName, action.password, state, false, true)
        }
        is SignUp -> {
            state = processStateForEmail(action.email, state, true)
            state = processStateForFirstName(action.firstName, state, true)
            state = processStateForLastName(action.lastName, state, true)
            state = processStateForPassword(action.firstName, action.lastName, action.password, state, true)
            state = processStateForBirthday(action.year, action.month, action.date, state)
            if(state.emailState == InputFieldState.VERIFIED && state.firstNameState == InputFieldState.VERIFIED && state.lastNameState == InputFieldState.VERIFIED && state.passwordState == InputFieldState.VERIFIED && state.bodYearState == InputFieldState.VERIFIED && state.bodMonthState == InputFieldState.VERIFIED && state.bodDateState == InputFieldState.VERIFIED){
                state = state.copy()
            }
        }
        is PasswordUpdate -> {
            state = processStateForEmail(action.email, state, false)
            state = processStateForFirstName(action.firstName, state, false)
            state = processStateForLastName(action.lastName, state, false)
            state = processStateForPassword(action.firstName, action.lastName, action.password, state, true)
        }
        is NoPromotionUpdate -> {
            state = processStateForNoPromotion(action.checked, state)
        }
        is BirthdayYearUpdate -> {
            state = resetBirthdayYearState(action.year, state)
        }
        is BirthdayMonthUpdate -> {
            state = resetBirthdayMonthState(action.month, state)
        }
        is BirthdayDateUpdate -> {
            state = resetBirthdayDateState(action.date, state)
        }
    }
    return state
}

fun resetBodErrorMsg(state: AppState){
    state.bodErrorMsg = null
}

fun resetBirthdayYearState(year: Int, state: AppState): AppState{
    if(year != 0 && state.bodMonth != 0 && state.bodDate != 0){
        resetBodErrorMsg(state)
    }
    return state.copy(bodYear = year, bodYearState = InputFieldState.INIT)
}

fun resetBirthdayMonthState(month: Int, state: AppState): AppState{
    if(month != 0 && state.bodYear != 0 && state.bodDate != 0){
        resetBodErrorMsg(state)
    }
    return state.copy(bodMonth = month, bodMonthState = InputFieldState.INIT)
}

fun resetBirthdayDateState(date: Int, state: AppState): AppState{
    if(date != 0 && state.bodMonth != 0 && state.bodYear != 0){
        resetBodErrorMsg(state)
    }
    return state.copy(bodDate = date, bodDateState = InputFieldState.INIT)
}

fun resetEmailState(email: String, state: AppState): AppState{
    return state.copy(email = email, emailErrorMsg = null, emailState = InputFieldState.INIT)
}

fun resetFirstNameState(firstName: String, state: AppState): AppState{
    return state.copy(firstName = firstName, firstNameErrorMsg = null, firstNameState = InputFieldState.INIT)
}

fun resetLastNameState(lastName: String, state: AppState): AppState{
    return state.copy(lastName = lastName, lastNameErrorMsg = null, lastNameState = InputFieldState.INIT)
}

fun processStateForNoPromotion(stopPromotion: Boolean, state: AppState):AppState{
    return state.copy(noPromotion= stopPromotion)
}

fun processStateForEmail(email: String, state: AppState, shouldUpdateEmailState: Boolean = true): AppState {
    var emailState: InputFieldState
    var emailErrorMessage: String? = null
    if (email.trim().isEmpty()) {
        emailState = InputFieldState.ERROR
        emailErrorMessage = "Please input email"
    } else if (isValidEmail(email)) {
        emailState = InputFieldState.VERIFIED
        emailErrorMessage = null
    } else {
        emailState = InputFieldState.ERROR
        emailErrorMessage = "Please input email in correct format"
    }
    if(shouldUpdateEmailState) {
        return state.copy(email = email, emailState = emailState, emailErrorMsg = emailErrorMessage)
    }else{
        return state.copy(email = email)
    }
}

fun processStateForFirstName(firstName: String, state: AppState, shouldUpdateFirstNameState: Boolean = true): AppState {
    var firstNameState: InputFieldState
    var firstNameErrorMessage: String? = null
    if (firstName.trim().isEmpty()) {
        firstNameState = InputFieldState.ERROR
        firstNameErrorMessage = "Please input first name"
    } else {
        firstNameState = InputFieldState.VERIFIED
        firstNameErrorMessage = null
    }
    if(shouldUpdateFirstNameState) {
        return state.copy(firstName = firstName, firstNameState = firstNameState, firstNameErrorMsg = firstNameErrorMessage)
    } else {
        return state.copy(firstName = firstName)
    }
}

fun processStateForLastName(lastName: String, state: AppState, shouldUpdateLastNameState: Boolean = true): AppState {
    var lastNameState: InputFieldState
    var lastNameErrorMessage: String? = null
    if (lastName.trim().isEmpty()) {
        lastNameState = InputFieldState.ERROR
        lastNameErrorMessage = "Please input first name"
    } else {
        lastNameState = InputFieldState.VERIFIED
        lastNameErrorMessage = null
    }
    if(shouldUpdateLastNameState) {
        return state.copy(lastName = lastName, lastNameState = lastNameState, lastNameErrorMsg = lastNameErrorMessage)
    }else{
        return state.copy(lastName = lastName)
    }
}

fun processStateForPassword(firstName: String, lastName: String, password: String, state: AppState, shouldUpdatePasswordState: Boolean = true, updateOnlyVerifiedState:Boolean = false): AppState {
    var passwordState: InputFieldState = InputFieldState.VERIFIED
    val hasSymbol = Regex("[-!\$%^&*()_+|~=`{}\\[\\]:\";'<>?,.\\/]").containsMatchIn(password)
    val hasNumbers = Regex("[0-9]").containsMatchIn(password)
    val passwordStrength = PasswordStrength.calculate(password)
    val containsName = password.isEmpty() || (lastName.isNotEmpty() && password.toLowerCase().indexOf(lastName.toLowerCase()) >= 0) || (firstName.isNotEmpty() && password.toLowerCase().indexOf(firstName.toLowerCase()) >= 0)
    val lessThan8Characters = password.length < 8
    val ssBuilder = SpannableStringBuilder()

    if(!updateOnlyVerifiedState && password.isEmpty()){
        passwordState = InputFieldState.ERROR
        ssBuilder.append(generateColorString(Color.RED, "Password is required."))
        return state.copy(password = password, passwordState = passwordState, passwordErrorMsg = ssBuilder.toSpannable())
    }

    if(passwordStrength == PasswordStrength.WEAK) {
        passwordState = InputFieldState.ERROR
        ssBuilder.append(generateColorString(Color.RED, "Password strength: weak\n"))
    } else {
        ssBuilder.append(generateColorString(Color.GREEN, "Password strength: ${passwordStrength.msg}\n"))
    }

    if(containsName){
        passwordState = InputFieldState.ERROR
        ssBuilder.append(generateColorString(Color.RED,"Password cannot contains your name\n"))
    } else {
        ssBuilder.append(generateColorString(Color.GREEN, "Password does not contains your name\n"))
    }

    if(lessThan8Characters){
        passwordState = InputFieldState.ERROR
        ssBuilder.append(generateColorString(Color.RED,"Password must be at least 8 characters\n"))
    } else {
        ssBuilder.append(generateColorString(Color.GREEN, "Password is longer than 8 characters\n"))
    }

    if(!hasSymbol && !hasNumbers){
        passwordState = InputFieldState.ERROR
        ssBuilder.append(generateColorString(Color.RED,"Password must contains a number or symbol\n"))
    } else {
        ssBuilder.append(generateColorString(Color.GREEN,"Password has number(s) or symbol(s)\n"))
    }

    if(passwordState == InputFieldState.VERIFIED){
        ssBuilder.clear()
        ssBuilder.append(generateColorString(Color.GREEN, "Password strength: ${passwordStrength.msg}\n"))
    }

    if((updateOnlyVerifiedState && state.passwordState == InputFieldState.VERIFIED) || shouldUpdatePasswordState){
        return state.copy(password = password, passwordState = passwordState, passwordErrorMsg = ssBuilder.toSpannable())
    }else{
        return state.copy(password = password)
    }

}

fun processStateForBirthday(year: Int, month: Int, date: Int, state: AppState): AppState {
    val yearState = if (year == 0) InputFieldState.ERROR else InputFieldState.VERIFIED
    val monthState = if (month == 0) InputFieldState.ERROR else InputFieldState.VERIFIED
    val dateState = if (date == 0) InputFieldState.ERROR else InputFieldState.VERIFIED
    var bodErrorMsg: String? = null
    if(year == 0 || month == 0 || date == 0){
        bodErrorMsg = "Select your birth date to continue."
    }
    return state.copy(
        bodYear = year,
        bodMonth = month,
        bodDate = date,
        bodYearState = yearState,
        bodMonthState = monthState,
        bodDateState = dateState,
        bodErrorMsg = bodErrorMsg
    )
}

fun generateColorString(@ColorInt color: Int, string: String): SpannableString{
    val output = SpannableString(string)
    output.setSpan(ForegroundColorSpan(color), 0, string.length, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE)
    return output
}

fun isValidEmail(target: CharSequence): Boolean {
    return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
}