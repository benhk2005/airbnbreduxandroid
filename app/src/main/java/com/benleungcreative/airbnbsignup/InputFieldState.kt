package com.benleungcreative.airbnbsignup

enum class InputFieldState {

    INIT, ERROR, VERIFIED

}