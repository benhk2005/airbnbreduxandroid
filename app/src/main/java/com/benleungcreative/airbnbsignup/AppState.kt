package com.benleungcreative.airbnbsignup

import android.text.Spannable
import org.rekotlin.StateType

data class AppState(
    var email: String = "",
    var emailState: InputFieldState = InputFieldState.INIT,
    var emailErrorMsg: String? = null,
    var firstName: String = "",
    var firstNameState: InputFieldState = InputFieldState.INIT,
    var firstNameErrorMsg: String? = null,
    var lastName: String = "",
    var lastNameState: InputFieldState = InputFieldState.INIT,
    var lastNameErrorMsg: String? = null,
    var password: String = "",
    var passwordState: InputFieldState = InputFieldState.INIT,
    var passwordErrorMsg: Spannable? = null,
    var bodYear: Int = 0,
    var bodYearState: InputFieldState = InputFieldState.INIT,
    var bodMonth: Int = 0,
    var bodMonthState: InputFieldState = InputFieldState.INIT,
    var bodDate: Int = 0,
    var bodDateState: InputFieldState = InputFieldState.INIT,
    var bodErrorMsg: String? = null,
    var noPromotion: Boolean = false,
    var showPassDialog: Boolean = false
) : StateType
