package com.benleungcreative.airbnbsignup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.rekotlin.Action
import org.rekotlin.Store
import org.rekotlin.StoreSubscriber

val mainStore = Store(
    reducer = ::mainReducer,
    state = null
)

class MainActivity : AppCompatActivity(), StoreSubscriber<AppState> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        signUpButton.setOnClickListener {
            mainStore.dispatch(
                SignUp(
                    emailEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    passwordEditText.text.toString(),
                    birthdayYearSpinner.selectedItemPosition,
                    birthdayMonthSpinner.selectedItemPosition,
                    birthdayDateSpinner.selectedItemPosition
                )
            )
        }

        emailEditText.addTextChangedListener(object : DispatchingTextWatcher() {
            override fun getDispatchingAction(): Action {
                return EmailUpdate(
                    emailEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }

        })

        firstNameEditText.addTextChangedListener(object : DispatchingTextWatcher() {
            override fun getDispatchingAction(): Action {
                return FirstNameUpdate(
                    emailEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }

        })

        lastNameEditText.addTextChangedListener(object : DispatchingTextWatcher() {
            override fun getDispatchingAction(): Action {
                return LastNameUpdate(
                    emailEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }

        })

        passwordEditText.addTextChangedListener(object : DispatchingTextWatcher() {
            override fun getDispatchingAction(): Action {
                return PasswordUpdate(
                    emailEditText.text.toString(),
                    firstNameEditText.text.toString(),
                    lastNameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }

        })

        noPromoCheckBox.setOnCheckedChangeListener { compoundButton, b ->
            mainStore.dispatch(NoPromotionUpdate(b))
        }

        birthdayMonthSpinner.onItemSelectedListener =
            object : DispatchingItemSelectListener() {
                override fun getDispatchingAction(index: Int): Action {
                    return BirthdayMonthUpdate(index)
                }

            }

        birthdayYearSpinner.onItemSelectedListener =
            object : DispatchingItemSelectListener() {
                override fun getDispatchingAction(index: Int): Action {
                    return BirthdayYearUpdate(index)
                }

            }

        birthdayDateSpinner.onItemSelectedListener =
            object : DispatchingItemSelectListener() {
                override fun getDispatchingAction(index: Int): Action {
                    return BirthdayDateUpdate(index)
                }

            }

        mainStore.subscribe(this)
    }

    override fun newState(state: AppState) {
        if (!state.email.equals(emailEditText.text.toString())) {
            emailEditText.setText(state.email)
        }
        errorMessageDisplayHandling(emailErrorMsg, state.emailErrorMsg)
        if (!state.firstName.equals(firstNameEditText.text.toString())) {
            firstNameEditText.setText(state.firstName)
        }
        errorMessageDisplayHandling(firstNameErrorMsg, state.firstNameErrorMsg)
        if (!state.lastName.equals(lastNameEditText.text.toString())) {
            lastNameEditText.setText(state.lastName)
        }
        errorMessageDisplayHandling(lastNameErrorMsg, state.lastNameErrorMsg)
        if (!state.password.equals(passwordEditText.text.toString())) {
            passwordEditText.setText(state.password)
        }
        passwordErrorMsg.text = state.passwordErrorMsg
        noPromoCheckBox.isChecked = state.noPromotion
        errorBackgroundDisplay(emailEditText, state.emailState)
        errorBackgroundDisplay(firstNameEditText, state.firstNameState)
        errorBackgroundDisplay(lastNameEditText, state.lastNameState)
        errorBackgroundDisplay(passwordEditText, state.passwordState)
        errorBackgroundDisplay(birthdayYearSpinner, state.bodYearState)
        errorBackgroundDisplay(birthdayMonthSpinner, state.bodMonthState)
        errorBackgroundDisplay(birthdayDateSpinner, state.bodDateState)
        if (state.bodYear != birthdayYearSpinner.selectedItemPosition) {
            birthdayYearSpinner.setSelection(state.bodYear)
        }
        if (state.bodMonth != birthdayMonthSpinner.selectedItemPosition) {
            birthdayYearSpinner.setSelection(state.bodMonth)
        }
        if (state.bodDate != birthdayDateSpinner.selectedItemPosition) {
            birthdayYearSpinner.setSelection(state.bodDate)
        }
        errorMessageDisplayHandling(birthdayErrorMsg, state.bodErrorMsg)
    }

    fun errorBackgroundDisplay(view: View, state: InputFieldState) {
        when (state) {
            InputFieldState.ERROR -> {
                view.setBackgroundResource(R.drawable.background_border_error)
            }
            else -> {
                view.setBackgroundResource(R.drawable.background_border_normal)
            }
        }
    }

    fun errorMessageDisplayHandling(errorMsgTextView: TextView, errorMsg: String?) {
        if ((errorMsg ?: "").trim().isNotEmpty()) {
            errorMsgTextView.visibility = View.VISIBLE
            errorMsgTextView.text = errorMsg
        } else {
            errorMsgTextView.text = ""
            errorMsgTextView.visibility = View.GONE
        }
    }

    inner abstract class DispatchingItemSelectListener : AdapterView.OnItemSelectedListener {

        abstract fun getDispatchingAction(index: Int): Action

        override fun onNothingSelected(p0: AdapterView<*>?) {

        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
            mainStore.dispatch(getDispatchingAction(position))
        }
    }

    inner abstract class DispatchingTextWatcher : TextWatcher {

        abstract fun getDispatchingAction(): Action

        override fun afterTextChanged(p0: Editable?) {
            mainStore.dispatch(getDispatchingAction())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }


}
