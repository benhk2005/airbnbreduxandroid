package com.benleungcreative.airbnbsignup

data class ErrorMsgEntry (var isError: Boolean = true, var errorMsg: String)