package com.benleungcreative.airbnbsignup

import org.rekotlin.Action

data class SignUp(
    val email: String,
    val firstName: String,
    val lastName: String,
    val password: String,
    val year: Int,
    val month: Int,
    val date: Int
) : Action

data class PasswordUpdate(val email: String, val firstName: String, val lastName: String, val password: String) : Action
data class EmailUpdate(val email: String, val firstName: String, val lastName: String, val password: String) : Action
data class FirstNameUpdate(val email: String, val firstName: String, val lastName: String, val password: String) :
    Action

data class LastNameUpdate(val email: String, val firstName: String, val lastName: String, val password: String) : Action
data class NoPromotionUpdate(val checked: Boolean) : Action
data class BirthdayYearUpdate(val year: Int) : Action
data class BirthdayMonthUpdate(val month: Int) : Action
data class BirthdayDateUpdate(val date: Int) : Action
