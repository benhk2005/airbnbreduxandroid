package com.benleungcreative.airbnbsignup.helper

import android.graphics.Color


//Copied from https://medium.com/@ssaurel/develop-a-password-strength-calculator-application-for-android-de3711ba7959

enum class PasswordStrength private constructor(var msg: String, var color: Int) {

    // we use some color in green tint =>
    //more secure is the password, more darker is the color associated
    WEAK("weak", Color.parseColor("#61ad85")),
    MEDIUM("medium", Color.parseColor("#4d8a6a")),
    STRONG("strong", Color.parseColor("#3a674f")),
    VERY_STRONG("very strong", Color.parseColor("#264535"));


    companion object {
        private val MIN_LENGTH = 8
        private val MAX_LENGTH = 15

        fun calculate(password: String): PasswordStrength {
            var score = 0
            // boolean indicating if password has an upper case
            var upper = false
            // boolean indicating if password has a lower case
            var lower = false
            // boolean indicating if password has at least one digit
            var digit = false
            // boolean indicating if password has a leat one special char
            var specialChar = false

            for (i in 0 until password.length) {
                val c = password[i]

                if (!specialChar && !Character.isLetterOrDigit(c)) {
                    score++
                    specialChar = true
                } else {
                    if (!digit && Character.isDigit(c)) {
                        score++
                        digit = true
                    } else {
                        if (!upper || !lower) {
                            if (Character.isUpperCase(c)) {
                                upper = true
                            } else {
                                lower = true
                            }

                            if (upper && lower) {
                                score++
                            }
                        }
                    }
                }
            }

            val length = password.length

            if (length > MAX_LENGTH) {
                score++
            } else if (length < MIN_LENGTH) {
                score = 0
            }

            // return enum following the score
            when (score) {
                0 -> return WEAK
                1 -> return MEDIUM
                2 -> return STRONG
                3 -> return VERY_STRONG
            }

            return VERY_STRONG
        }
    }
}